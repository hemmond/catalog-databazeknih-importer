// ==UserScript==
// @name     DatabazeKnih to Catalog
// @version  1.2.1
// @grant    none
// @match http://yourdomain.tld/add
// @require https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js
// @description Fill add book form data from databazeknih.cz
// ==/UserScript==

/* Created by hemmond, 2020 and Released under MIT license. */

function processData(data) {
  var dbk_book_id = $(data).find("[itemprop='description']").find("a").attr("id");

  if(dbk_book_id == null) { 
  	dbk_book_id = $(data).find("span#abinfo").attr("bid");
  }
  
  // ISBN and language are in "more data" section and are pulled separately via AJAX. 
  loadData("https://www.databazeknih.cz/books/book-detail-more-info-ajax.php?bid="+dbk_book_id, function(mdata, textStatus) {
    mdata = "<div>"+mdata+"</div>";
    $(".add-item-input[name='isbn']").val($(mdata).find("[itemprop='isbn']").text());  
    $(".add-item-input[name='language']").val($(mdata).find("[itemprop='language']").text());
  });
  
  // Title
  $(".add-item-input[name='title']").val($(data).find("[itemprop='name']").text());
	
  // Authors
  var authors = "";
  $(data).find("[itemprop='author']").find("a").each(function() {
    if(authors.length > 0) {
    	//add semicolon between authors
      authors += ";"
    }
    authors += $(this).text()
  });
  $(".add-item-input[name='author']").val(authors);
  
  // Genre, publisher, year and image path
  $(".add-item-input[name='publisher']").val($(data).find("[itemprop='publisher']").text());
  $(".add-item-input[name='year']").val($(data).find("[itemprop='datePublished']").text());
  $(".add-item-input[name='genre']").val($(data).find("[itemprop='genre']").text());
  $(".add-item-input[name='imgpath']").val($(data).find(".kniha_img").attr("src"));

  // Description
  var descriptionText = "";
  $(data).find("[itemprop='description']").find("span").each(function() {
  	descriptionText += $(this).text();
  });
  
  if(descriptionText.length <=1) {
  	$(data).find("[itemprop='description']").each(function() {
      descriptionText += $(this).text();
    });
  }
  
  $(".add-item-input[name='description']").val(descriptionText);

  // Original name
  var orig_title_full = $(data).find("span:contains(Originální název:)").next().text()
  var orig_title = orig_title_full.substring(0, orig_title_full.lastIndexOf(","));
  
  // Fix if date of publishing of the original is missing.
  if(orig_title.length <1) {
  	orig_title = orig_title_full;
  }
  
  $(".add-item-input[name='o_title']").val(orig_title.trim());
  
  // Series name and volume
  var series_field = $(data).find("span:contains(Série:)").next();
  var series_name = series_field.find("a").text();
  var series_volume = series_field.find("em").text();
  
  $(".add-item-input[name='series']").val(series_name.trim());
  $(".add-item-input[name='volume']").val(series_volume.trim().replace(/\D$/, '') );
};

function loadData(req_url, callback) {
  //console.log("Load data INIT: "+req_url);
  $.ajax({
    type: "GET",
    url: req_url,
    success: callback
  });
};

function databazeKnihSearchProcess() {
  var searchQuery = $('#databazeknih_search_query').val();
  
  if(searchQuery.startsWith("http")) {
    var searchURL = searchQuery;
  	console.log('Direct URL: '+searchURL);
  }
  else {
  	var searchURL = "https://www.databazeknih.cz/search?q="+searchQuery+"&hledat="
  	console.log('search triggered: '+searchURL);
  }
	
  loadData(searchURL, function(data, textStatus) {
    //console.log(data);
    //console.log(textStatus);
    processData(data);
  });
};

var script = document.createElement('script');
script.type = "text/javascript"; 

// Register functions to be injected here. 
script.innerHTML = databazeKnihSearchProcess +";\n"; 
script.innerHTML += loadData+";\n"; 
script.innerHTML += processData+";\n"; 

document.getElementsByTagName('head')[0].appendChild(script);

//console.log(script.innerHTML);
//=============================================================

// Redirect to added book:
if($("#item-list").find("form").length == 0) {
	window.location.replace($("#item-list p a").attr("href"));
}
else {

  //=============================================================

  queryInputField = `
	<form id="databazeknih_search_form" action="#" onsubmit="return false;">
		<label class="add-new-item">Databaze knih (barcode/url) search query:</label>
		<input class="add-item-input" type="text" id="databazeknih_search_query" name="databazeknih_search_query" autofocus />
	</form>`;
  $("#item-list").html(queryInputField + $("#item-list").html() );

  $("#databazeknih_search_form").submit(function(event) { databazeKnihSearchProcess();});

} //end else of redirect to added book. 
//=============================================================
