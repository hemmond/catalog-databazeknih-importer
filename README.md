# Catalog databazeknih.cz importer

This script allows user to search for the book on server databazeknih.cz and fills all available fields from that server.

This script is intended for firefox with extension Greasemonkey (for running javascript code). 


## Detailed info

Server databazeknih.cz automatically redirects if exact match is found (for example correct full name without ambiguity or ISBN number). 
ISBN number is accepted (and preffered) as ISBN number is also book barcode number and therefore can be read by barcode reader. 
Server databazeknih.cz does not need dashes in ISBN to find exact match. 
I really recomend using barcode reader for this (all barcode readers I have encountered works as numeric keyboard and usualy type the numbers and sends enter - submits form). 

## Running the script

Add the content of userscript.js to Greasemonkey and replace yourdomain.tld in @match section with URL where your catalog is running. 
Without proper URL, greasemonkey will not know that the code should be run on this page and the code will not work. 

## Useful links

Greasemonkey Firefox addon: https://addons.mozilla.org/cs/firefox/addon/greasemonkey/

Adam Paszternak's catalog: https://github.com/psztrnk/catalog

## License

This script is released under the MIT License (same as Catalog itself). Full text of the license can be found in the `LICENSE` file.
